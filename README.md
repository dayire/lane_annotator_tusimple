# README #

### What is this repository for? ###

* this SW allows to make instance lane annotation for a set of given images
* some bugs may happen, if so please reach out to me
* SW is functional and sanity checking is available during annotation
* v1
* runs on python 3.7 and needs:
* opencv2, glob, numpy, os

### How do I use it? ###

* better make annotation with two screens!!!
* 1 when running this SW, you will have two windows, one showing the frame and the other one the mask
* 2 left click to set a point on the frame
* 3 after setting a number of points, press "d" to fit a line
* 4 set new points for the same lane and press "d" to fit a another line if necessary
* 5 press "n" to set a new id for another lane instance, you should see on the console the new_id and a message about the color change
* 6 press "b" to go back to the previous instance id, color may get buggy on the image but always make sure that the one on the mask correspond
* 6 for the second lane, do 2,3,4. You will notice that a different color is used on the image, as well as on the mask
* 7 press "t" to reset a misclick or a bad drawn fitted line(when you set some points and you already pressed "d"), please note that you will have to keep on pressing "t" until all new point disapear
* 8 press "r" to reset the frame and the current mask and clean everything (for the current frame/mask)
* 9 press "c" for the next frame/image
* 10 press "q" to exit the SW and save the numpy masks


# INPUT:
* a set of tobi driving images
* set the path to the folder you're working on, must contain images
* file_id is how you would like to name the .npy masks


# OUTPUT: 
* numpy file containing all masks 
* the original frame with the user annotation
* the mask with values from 0:backround + 1 to n with 1 is the first lane instance pixel, 2, the second etc...
* the safety mask just for vizuation which is the mask * 50 for vizuation (this is the one that will be displayed to user

### Who do I talk to? ###
* Rachid: s.rachid.riad@gmail.com


# Always use the window of the mask to make sure everything is correct
# WHEN YOU FINISH ANNOTATION, ALWAYS PRESS "q" so the masks will be saved!!!!
