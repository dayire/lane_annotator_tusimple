import glob
import os

import cv2
import numpy as np

refPt = []
click_points = []
cropping = False
pointing = False
bandwidth = 20

image = None

# TODO set this to the folder you're working on, must contain images
# TODO file_id is how you would like to name the .npy masks

path_tobi = "C:/Users/s_rac/Downloads/tobii_lane/1/*"
saving_folder = "C:/Users/s_rac/Downloads/tobii_lane/"
file_id = "tobi_1"


def click_and_crop(event, x, y, flags, param):
    # grab references to the global   variables
    global refPt, cropping, click_points, pointing

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True
        pointing = True

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        click_points.append(refPt[0])
        cropping = False

# function that fits a parabola through the points and draw them on image for visuationion and on the mask
def draw_poly(frame, iter_id, rgb, mask_current, points):
    # print("the fucking points ", points)
    lane_x = points[:, 0]
    lane_y = points[:, 1]

    top = lane_y.min()
    bottom = lane_y.max()
    ploty_y = np.linspace(top, bottom, bottom - top)
    lane_fit_coeffs = np.polyfit(lane_y, lane_x, 2)

    lane_fit_x = lane_fit_coeffs[0] * ploty_y ** 2 + lane_fit_coeffs[1] * ploty_y + lane_fit_coeffs[2]

    # if we want to use degree 2 use this
    # lane_fit_x = lane_fit_coeffs[0] * ploty_y ** 3 + lane_fit_coeffs[1] * ploty_y  ** 2 + lane_fit_coeffs[2] * ploty_y + lane_fit_coeffs[3]

    x0 = lane_fit_x.astype(np.int)[0]
    y0 = ploty_y.astype(np.int)[0]
    for y, x in zip(ploty_y.astype(np.int), lane_fit_x.astype(np.int)):
        # frame = cv2.circle(frame, (x, y), 0, (rgb[0], rgb[2], rgb[1]), 10)
        # mask_current = cv2.circle(mask_current, (x, y), 0, iter_id, 10)
        frame = cv2.line(frame, (x0, y0), (x, y), (rgb[0], rgb[2], rgb[1]), 10)
        mask_current = cv2.line(mask_current, (x0, y0), (x, y), iter_id, 10)

        x0 = x
        y0 = y

    return frame, mask_current


# main function that process frame by frame
def process_frame(frame):
    global bandwidth
    global click_points
    global pointing

    # for debuging
    cv2.namedWindow("frame_mask", cv2.WINDOW_NORMAL)
    cv2.moveWindow("frame_mask", 800, 0)

    cv2.namedWindow("image", cv2.WINDOW_NORMAL)
    cv2.moveWindow("image", 0, 0)
    cv2.setMouseCallback("image", click_and_crop)

    mask_current = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.uint8)

    end = False

    # initialize iterator over the instance lane id and the color switch
    iter_mask = 1
    iter_lane_r = 0
    iter_lane_g = 255
    iter_lane_b = 0
    rgb = [iter_lane_r, iter_lane_g, iter_lane_b]

    # saving the initial frames frame
    frame_save = frame.copy()
    mask_save = mask_current.copy()

    # where frames will be saved to use the "t" option
    list_lanesPoints = []

    # WARNING!! INIFITE LOOP
    while True:


        if not cropping:
            oldframe = frame.copy()

            for i, center in enumerate(click_points):
                frame = cv2.circle(frame, center, 2, (0, 255, 0), 2)
                if np.array_equal(oldframe, frame) != True:
                    list_lanesPoints.append(oldframe)

            cv2.imshow('frame_mask', 50 * mask_current)
            cv2.imshow('image', frame)

        key = cv2.waitKey(1) & 0xFF

        # print("len of that shit", len(list_lanesPoints))
        if key == ord("t"):
            mask_current = mask_prev.copy()
            click_points = click_points[:-1]
            frame = list_lanesPoints[-1].copy()
            list_lanesPoints = list_lanesPoints[:-1]

            cv2.imshow('image', frame)
            cv2.imshow('frame_mask', 70 * mask_current)

            print(" LANE COLOUR SWITCHED".format(rgb))

        # b pressed for resesting the lane
        if key == ord("b"):
            iter_mask -= 1
            rgb[0] -= 30
            rgb[1] = 0
            rgb[2] -= 30
            print("****** CANCELLED CHANGE AND BACK TO OLD ID: ")
            print(" MASK ID LANE {} ".format(iter_mask))
            print("****** LANE COLOUR SWITCHED RGB {}".format(rgb))

        # draw the current lane on the points
        elif key == ord("d"):
            mask_prev = mask_current.copy()

            if click_points:
                frame_mask = frame.copy()
                pointing = False
                points = np.asarray(click_points)

                frame, mask_current = draw_poly(frame, iter_mask, rgb, mask_current, points)

                click_points = []

        # start a new id for new lane and new color
        # prepare the next id for the new lane instance
        # working
        elif key == ord("n"):
            iter_mask += 1
            rgb[0] += 30
            rgb[1] = 0
            rgb[2] += 30
            print("****** NEW MASK ID LANE {} ".format(iter_mask))
            print("****** LANE COLOUR SWITCHED RGB {}", rgb)

        # used to reset the whole frame
        # working
        elif key == ord("r"):
            frame = frame_save.copy()
            mask_current = mask_save.copy()
            iter_mask = 1
            iter_lane_r = 0
            iter_lane_g = 255
            iter_lane_b = 0
            print("RESETED FRAME and MASK")
            print("id_ lane instance {} ".format(iter_mask))


        # if q is pressed, quit the SW
        # quit
        elif key == ord("q"):
            end = True
            break
        # if the 'c' key is pressed, break from the loop
        # next image
        elif key == ord("c"):
            break

    click_points = []

    return end, frame, mask_current


def main():
    masks = []
    frames = []

    list_imgs = glob.glob(path_tobi)

    for img_path in sorted(list_imgs):
        # print(img_path)
        image_id = os.path.basename(img_path)[:-4]

        frame = cv2.imread(img_path)

        try:
            end, frame, mask = process_frame(frame)


        except:
            print("ERROR HAPPEND!!!")
            continue

        cv2.destroyAllWindows()

        # saving individual images and masks for safey
        cv2.imwrite(saving_folder + image_id + "_annot_frame.png", frame)
        cv2.imwrite(saving_folder + image_id + "_mask.png", mask)

        safety_mask = 25 * mask
        cv2.imwrite(saving_folder + image_id + "_mask_safety.png", safety_mask)

        masks.append(mask)
        frames.append(frame)

        if end:
            break

    np.save(saving_folder + file_id + '_masks.npy', np.asarray(masks))


if __name__ == "__main__":
    main()

